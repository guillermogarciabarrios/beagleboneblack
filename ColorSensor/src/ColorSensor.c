/*
 * Authors: Jaime Sancho & Guillermo García
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

int main(void) {

	int file;
	int addr = 0x29;
	char buffRead[2];
	char buffWrite[2];
	short blue, red, green;

	// Open interface

	file = open("/dev/i2c-1", O_RDWR);
	if(file < 0){
		perror(" Interface not available\n");
		exit(1);
	}

	// Open connection with the device

	if(ioctl(file, I2C_SLAVE, addr) < 0){
		perror(" Sensor not available");
		exit(1);
	}

	// Initialize the sensor

	buffWrite[0] = 0x00 | 0x80;		// Command with register target 0x00
	buffWrite[1] = 0x0B;			// Enable WEN AEN PON

	if(write(file, buffWrite, 2) != 2)
		perror(" Not able to initialize");
	else
		printf("Sensor initialized");

	// Reading colors


	printf("\n");

	while(1){
		usleep(500000); // 0.5 sec

		buffWrite[0] = 0x1A | 0xA0;		// Blue register

		if(write(file, buffWrite, 1) != 1)
			perror(" Not able to access to blue register");
		else {

			if(read(file, buffRead, 2) != 2){
				perror(" Not able to read from blue register");
			} else{
				blue = ((buffRead[1] << 8) | buffRead[0]);
				//printf("The blue measure is: %d\n", blue);
			}
		}

		buffWrite[0] = 0x16 | 0xA0;		// Red register

		if(write(file, buffWrite, 1) != 1)
			perror(" Not able to access to red register");
		else {

			if(read(file, buffRead, 2) != 2){
				perror(" Not able to read from red register");
			} else{
				red = ((buffRead[1] << 8) | buffRead[0]);
				//printf("The red measure is: %d\n", red);
			}
		}

		buffWrite[0] = 0x18 | 0xA0;		// Green register

		if(write(file, buffWrite, 1) != 1)
			perror(" Not able to access to green register");
		else {

			if(read(file, buffRead, 2) != 2){
				perror(" Not able to read from green register");
			} else{
				green = ((buffRead[1] << 8) | buffRead[0]);
				//printf("The green measure is: %d\n", green);
			}
		}

		if(blue > green && blue > red)
			printf("BLUE\n");
		else if(green > blue && green > red)
			printf("GREEN\n");
		else if(red > blue && red > green)
			printf("RED\n");

	}
	return 0;
}
