/*
 * Authors: Jaime Sancho & Guillermo García
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

const int THRESHOLD = 700;

const char CTRL_REG1 = 0x20;
const char STATUS_REG = 0x27;


const char OUT_X_L = 0x28;
const char OUT_X_H = 0x29;
const char OUT_Y_L = 0x2A;
const char OUT_Y_H = 0x2B;
const char OUT_Z_L = 0x2C;
const char OUT_Z_H = 0x2D;



short readAxis(int file, char low, char high){

	char buffRead[2];
	char buffWrite[2];
	short value;

	// Read low

	buffWrite[0] = low;

	if(write(file, buffWrite, 1) != 1)
		perror(" Not able to access to register");
	else {
		if(read(file, buffRead, 1) != 1)
			perror(" Not able to read from low register");
		else
			value = buffRead[0];
	}

	// Read high

	buffWrite[0] = high;

	if(write(file, buffWrite, 1) != 1)
		perror(" Not able to access to  register");
	else {
		if(read(file, buffRead, 1) != 1)
			perror(" Not able to read from high register");
	}

	value = (buffRead[0] << 8) | value;

	return value;
}

int main(void) {

	int file;
	int addr = 0x6B;
	char buffRead[2];
	char buffWrite[2];
	short x, y, z;
	float r0x, r0y, r0z;
	float xConv, yConv, zConv;
	float convFactor = 8.75/1000;
	float period = 1.0/95;
	float xDisp, yDisp, zDisp;
	xDisp = 0;
	yDisp = 0;
	zDisp = 0;


	// Open interface

	file = open("/dev/i2c-2", O_RDWR);
	if(file < 0){
		perror(" Interface not available\n");
		exit(1);
	}

	// Open connection with the device

	if(ioctl(file, I2C_SLAVE, addr) < 0){
		perror(" Sensor not available");
		exit(1);
	}

	// Initialize the sensor

	buffWrite[0] = CTRL_REG1;
	buffWrite[1] = 0x0F;	// Enable PD Zen Xen Yen

	if(write(file, buffWrite, 2) != 2)
		perror(" Not able to initialize");
	else
		printf("Sensor initialized\n");


	// Calibration

	r0x = 0;
	r0y = 0;
	r0z = 0;
	int i;
	for(i = 0; i < 200; i++){
		r0x += readAxis(file, OUT_X_L, OUT_X_H);
		r0y += readAxis(file, OUT_Y_L, OUT_Y_H);
		r0z += readAxis(file, OUT_Z_L, OUT_Z_H);
	}

	r0x /= 200;
	r0y /= 200;
	r0z /= 200;

	printf("Calibration done\n ");

	while(1){

		// Read status

		buffWrite[0] = STATUS_REG;		// Check actualization

		if(write(file, buffWrite, 1) != 1)
			perror(" Not able to access to status register");
		else {
			if(read(file, buffRead, 1) != 1)
				perror(" Not able to read to status register");
		}

		if((buffRead[0] & 0x08) == 0x08){

			x = readAxis(file, OUT_X_L, OUT_X_H);
			y = readAxis(file, OUT_Y_L, OUT_Y_H);
			z = readAxis(file, OUT_Z_L, OUT_Z_H);


			// Threshold + conversion

			if(abs(x-r0x) > THRESHOLD)
				xConv = convFactor * x;
			else
				xConv = 0;

			if(abs(y-r0y) > THRESHOLD)
				yConv = convFactor * y;
			else
				yConv = 0;

			if(abs(z-r0z) > THRESHOLD)
				zConv = convFactor * z;
			else
				zConv = 0;

			// Integration

			xDisp += xConv * period;
			yDisp += yConv * period;
			zDisp += zConv * period;

			printf("Angular speed (X): %f  ", xConv);
			printf("(Y): %f  ", yConv);
			printf("(Z): %f | ", zConv);

			printf("Displacement (X): %f  ", xDisp);
			printf("(Y): %f  ", yDisp);
			printf("(Z): %f\n", zDisp);

		}
	}

	return 0;

}
